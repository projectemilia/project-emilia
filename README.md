<p align="center"><img src="https://i.imgur.com/0QdTJH9.png" alt="Emilia"></p>
<p align="center">
<strong>A fast and elegant Discord bot with many (planned) features, such as music, moderation and more!</strong><br>
Powered by Lavalink, MongoDB and Discord.js.
</p>

## Key Features
- Support for multiple music platforms
- Discord's new slash commands
- Clean embeds
- Easy to use
- Fast and simple

## Platform Support (Music)
- Spotify  
- Soundcloud (Currently broken, waiting for PR [#650](https://github.com/sedmelluq/lavaplayer/pull/650) to be merged)  
- YouTube
- More coming soon!

## Links
- [Invite Me](https://emilia.tkkr.tech/invite)
- [Discord Server](https://emilia.tkkr.tech/discord)
- [GitHub Repository](https://emilia.tkkr.tech/github)
- [Invite Me (with admin permissions)](https://emilia.tkkr.tech/admin)

## Contact
- Find me on Discord at `thaddeuskkr#4416`
- Send an email [here](mailto:emilia@tkkr.tech)
- Join our [support server](https://emilia.tkkr.tech/discord)

## Selfhosting
**Here's all the information you should need for self-hosting the bot.**  

### Command-line arguments
**`--development`|`-d`:** commands will only be registered for development server, faster than global  
**`--autoupdate`|`-au`:** sends update message when a version change is detected  
  
### Ignored files
`config.json`  
```json
{
    "devServer": "",
    "owners": [""],
    "color": "#c181e4",
    "link": "",
    "url": "",
    "logChannel": ""
}
```  
- **`devServer`:** a server ID, if the `--development` / `-d` flag is passed this makes the bot only register commands for this server  
- **`owners`:** an array of owners who can use owner-only commands  
- **`color`:** the color of all embeds  
- **`link`:** the text shown at the footer of most of the bot's embeds, after the bot's username  
- **`url`:** the url of the bot's website, used as author url  
- **`logChannel`:** logging channel, not currently used  
  
`.env`  
```env
TOKEN = ""
MONGO_URL = ""
LAVALINK_HOST = ""
LAVALINK_PORT = 
LAVALINK_PASS = ""
SPOTIFY_CLIENT_ID = ""
SPOTIFY_CLIENT_SECRET = ""
WEBHOOK_ID = ""
WEBHOOK_TOKEN = ""
```  
- **`TOKEN`:** your discord bot token  
- **`MONGO_URL`:** your mongodb database url, should contain all credentials  
- **`LAVALINK_HOST`:** lavalink server host (url)  
- **`LAVALINK_PORT`:** port on which your server is hosted on  
- **`LAVALINK_PASS`:** lavalink server password  
- **`SPOTIFY_CLIENT_ID`:** client id from [spotify developer dashboard](https://developer.spotify.com/dashboard/)  
- **`SPOTIFY_CLIENT_SECRET`:** client secret from [spotify developer dashboard](https://developer.spotify.com/dashboard/)  
- **`WEBHOOK_ID`:** a webhook id for logging, preferably in the same channel as logging in config  
- **`WEBHOOK_TOKEN`:** authentication for webhook specified previously  
