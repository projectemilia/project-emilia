const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'ping',
    description: 'Sends you the ping of the bot',
    options: [],
    run: async (client, interaction) => {
        const embed = new MessageEmbed()
            .setAuthor('Pong!', interaction.user.avatarURL(), client.cfg.url)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setDescription(`**API Latency:** ${Math.round(client.ws.ping)}ms`)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};