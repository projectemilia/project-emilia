const { MessageEmbed } = require('discord.js');
const fetch = require('node-fetch');
const { repository } = require('../../package.json');

module.exports = {
    name: 'github',
    description: 'Shows you information about Emilia\'s GitHub repository.',
    options: [],

    inVc: false,
    sameVc: false,
    dispatcher: false,
    permissions: [],

    run: async (client, interaction) => {
        let general = await fetch(`https://api.github.com/repos/${client.repository}`);
        general = await general.json();
        let commits = await fetch(`${general.commits_url.replace('{/sha}', '')}`);
        commits = await commits.json();
        const createdAt = new Date(general.created_at);
        const updatedAt = new Date(general.updated_at);
        const embed = new MessageEmbed()
            .setAuthor('GitHub じょうほう', null, repository.url.replace('git+', ''))
            .setColor(client.defaults.color)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setThumbnail(general.owner.avatar_url)
            .setDescription(`**[Link](${general.html_url})** | **[Homepage](${general.homepage})**`)
            .addFields(
                [
                    {
                        name: 'Name',
                        value: general.name,
                        inline: true
                    },
                    {
                        name: 'Owner',
                        value: `${general.owner.login} (\`${general.owner.id}\`)`,
                        inline: true
                    },
                    {
                        name: 'Description',
                        value: general.description
                    },
                    {
                        name: 'Created at',
                        value: createdAt.toString()
                    },
                    {
                        name: 'Last updated',
                        value: updatedAt.toString()
                    },
                    {
                        name: 'Clone command',
                        value: `\`git clone ${general.clone_url}\``
                    },
                    {
                        name: 'Last commit',
                        value: `[\`${commits[0].sha}\`](${commits[0].html_url}) (${commits[0].commit.committer.name})`
                    },
                    {
                        name: 'Last commit message',
                        value: `${commits[0].commit.message}`
                    }
                ]
            );  
        return await interaction.reply({ embeds: [embed], ephemeral: true });
    }
};