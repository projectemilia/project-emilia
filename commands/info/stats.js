const { MessageEmbed } = require('discord.js');
const os = require('os');
var osu = require('node-os-utils');
var cpu = osu.cpu;
const prettyms = require('pretty-ms');

module.exports = {
    name: 'stats',
    description: 'Shows detailed usage / statistics of the bot.',
    options: [],
    run: async (client, interaction) => {
        let totalCpuSpeed = 0;
        for (const cpu of os.cpus()) {
            totalCpuSpeed = totalCpuSpeed + cpu.speed;
        }
        const cpuSpeed = totalCpuSpeed / os.cpus().length;
        const embed = new MessageEmbed()
            .setAuthor('とうけい', null, client.cfg.url)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setDescription(
                `**» Uptime:** \`${prettyms(client.uptime)}\`\n` +
                '\n' +
                '__**Memory (RAM):**__\n' +
                `**» Free:** \`${(os.freemem / 1e+9).toFixed(2)} GB\`\n` +
                `**» Used:** \`${((os.totalmem-os.freemem) / 1e+9).toFixed(2)} GB\`\n` +
                `**» Total:** \`${(os.totalmem / 1e+9).toFixed(2)} GB\`\n` +
                '\n' +
                '__**CPU:**__\n' +
                `**\`${await cpu.count()}x ${await cpu.model()}\`**\n` +
                `**» Usage:** \`${await cpu.usage()}%\`\n` +
                `**» Speed:** \`${(cpuSpeed / 1000).toFixed(2)} GHz\`\n` +
                `**» Load average:** \`[ ${os.loadavg().join(', ')} ]\`\n` +
                `**» Load average (1 min):** \`${await cpu.loadavgTime()}\`\n` +
                '\n' +
                '__**Lavalink:**__\n' +
                `**» CPU load:** \`${client.shoukaku.getNode().stats.cpu.lavalinkLoad.toFixed(2)}\`\n` +
                `**» Memory usage:** \`${(client.shoukaku.getNode().stats.memory.used / 1e+9).toFixed(2)} GB / ${(client.shoukaku.getNode().stats.memory.allocated / 1e+9).toFixed(2)} GB\`\n` +
                `**» Frame stats:** \`${client.shoukaku.getNode().stats.frameStats.sent} sent, ${client.shoukaku.getNode().stats.frameStats.deficit} deficit, ${client.shoukaku.getNode().stats.frameStats.nulled} nulled\`\n` +
                `**» Total players:** \`${client.shoukaku.getNode().stats.players}\`\n` +
                `**» Playing players:** \`${client.shoukaku.getNode().stats.playingPlayers}\`\n` +
                `**» Uptime:** \`${prettyms(client.shoukaku.getNode().stats.uptime)}\`\n`
            )
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed] });
    }
};
