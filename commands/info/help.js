const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'help',
    description: 'Shows you a bunch of useful information about the bot, and gives a full list of commands.',
    options: [],

    inVc: false,
    sameVc: false,
    dispatcher: false,
    permissions: [],

    run: async (client, interaction) => {
        const embed = new MessageEmbed()
            .setAuthor('こんにちは!')
            .setDescription(
                'Emilia is a Discord bot with many features, powered by [discord.js](https://discord.js.org) v13 and [Lavalink](https://github.com/freyacodes/Lavalink).\n' +
                `Currently, Emilia has **${client.commands.size}** commands in ${client.categories.length} categories.\n` +
                'Type `/` to see a full list of my commands pop up in your text box.\n' +
                'When using Discord\'s new slash commands, just remember that the **TAB** button is quite useful.\n' +
                `[**Website**](${client.cfg.url}) | [**GitHub**](https://github.com/${client.repository}) | [**Invite**](https://emilia.tkkr.tech/invite) | [Support](https://discord.gg/cR4uGKjbgg)`
            )
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setImage(client.cfg.banner);
        return interaction.reply({ embeds: [embed] });
    }
};