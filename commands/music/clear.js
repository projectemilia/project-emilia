const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'clear',
    description: 'Clears the queue for the server.',
    options: [],

    inVc: true,
    sameVc: true,
    dispatcher: true,

    run: async (client, interaction, args, dispatcher) => {
        dispatcher.queue.length = 0;
        dispatcher.repeat = 'off';
        const embed = new MessageEmbed()
            .setAuthor('クリア', null, client.cfg.url)
            .setDescription(`Cleared the queue for <#${dispatcher.player.connection.channelId}>.`)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};