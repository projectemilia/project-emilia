const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'skip',
    description: 'Skips the currently playing track.',
    options: [],

    inVc: true,
    sameVc: true,
    dispatcher: true,

    run: async (client, interaction, args, dispatcher) => {
        const current = dispatcher.current.info;
        dispatcher.player.stopTrack();
        const embed = new MessageEmbed()
            .setAuthor('終わりました', null, client.cfg.url)
            .setDescription(`Skipped **[${current.title}](${current.uri})**.`)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};