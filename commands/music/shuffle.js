const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'shuffle',
    description: 'Shuffles the server\'s queue.',
    options: [],

    inVc: true,
    sameVc: true,
    dispatcher: true,
    permissions: [],

    run: async (client, interaction, args, dispatcher) => {
        dispatcher.queue = dispatcher.queue.sort(() => Math.random() - 0.5);
        const embed = new MessageEmbed()
            .setDescription(`Shuffled **${dispatcher.queue.length}** tracks. Use \`/queue\` to view the new queue.`)
            .setAuthor('シャッフル', null, client.cfg.url)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};