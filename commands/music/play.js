const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'play',
    description: 'Searches your query on YouTube and plays it in your voice channel.',
    options: [
        {
            name: 'query',
            description: 'What would you like to listen to?',
            type: 'STRING',
            required: true
        }
    ],
    
    inVc: true,
    sameVc: true,
    dispatcher: false,

    run: async (client, interaction, args) => {
        const query = args[0];

        const node = client.shoukaku.getNode();
        const lavasfy = client.lavasfy.getNode('Emilia');

        if (checkURL(query)) {
            if (query.includes('open.spotify.com')) {
                const res = await lavasfy.load(query);
                const tracks = res.tracks;
                const embed = new MessageEmbed()
                    .setAuthor('ごめん', null, client.cfg.url)
                    .setFooter(client.defaults.footer, client.defaults.avatar)
                    .setColor(client.defaults.color);
                switch (res.loadType) {
                case 'NO_MATCHES':
                    return interaction.reply({ embeds: [embed.setDescription('There were no results found for your search.')], ephemeral: true });
                case 'LOAD_FAILED':
                    return interaction.reply({ embeds: [embed.setDescription('An unknown error occurred:\n' + `\`${res.exception}\``)], ephemeral: true });
                default:
                    break;
                }
                const track = tracks.shift();
                const playlist = res.loadType === 'PLAYLIST_LOADED';
                const dispatcher = await client.queue.handle(interaction.guild, interaction.member, interaction.channel, node, track);
                let plLen = 0;
                if (playlist) {
                    for (const track of tracks) {
                        plLen++;
                        await client.queue.handle(interaction.guild, interaction.member, interaction.channel, node, track);
                    }
                }
                const sEmbed = new MessageEmbed()
                    .setDescription(`${playlist ? `Added the playlist **${res.playlistInfo.name}** with **${plLen}** tracks to the queue.` : `Added **${track.info.title}** to the queue.`}`)
                    .setColor(client.defaults.color);
                await interaction.reply({ embeds: [sEmbed], ephemeral: true });
                dispatcher?.play();
                return;
            }
            const result = await node.rest.resolve(query);
            if (!result) {
                const embed = new MessageEmbed()
                    .setAuthor('ごめん', null, client.cfg.url)
                    .setDescription('There were no results found for your search.')
                    .setFooter(client.defaults.footer, client.defaults.avatar)
                    .setColor(client.defaults.color);
                return interaction.reply({ embeds: [embed], ephemeral: true });
            }
            const { type, tracks, playlistName } = result;
            const track = tracks.shift();
            const playlist = type === 'PLAYLIST';
            const dispatcher = await client.queue.handle(interaction.guild, interaction.member, interaction.channel, node, track);
            let plLen = 0;
            if (playlist) {
                for (const track of tracks) {
                    plLen++;
                    await client.queue.handle(interaction.guild, interaction.member, interaction.channel, node, track);
                } 
            }
            const embed = new MessageEmbed()
                .setDescription(`${playlist ? `Added the playlist **${playlistName}** with **${plLen}** tracks to the queue.` : `Added **${track.info.title}** to the queue.`}`)
                .setColor(client.defaults.color);
            await interaction.reply({ embeds: [embed], ephemeral: true });
            dispatcher?.play();
            return;
        }
        const search = await node.rest.resolve(query, 'youtube');

        if (!search?.tracks.length) {
            const embed = new MessageEmbed()
                .setAuthor('ごめん', null, client.cfg.url)
                .setDescription('There were no results found for your search.')
                .setFooter(client.defaults.footer, client.defaults.avatar)
                .setColor(client.defaults.color);
            return interaction.reply({ embeds: [embed], ephemeral: true });
        }
        const track = search.tracks.shift();
        const dispatcher = await client.queue.handle(interaction.guild, interaction.member, interaction.channel, node, track);
        const embed = new MessageEmbed()
            .setDescription(`Added **${track.info.title}** to the queue.`)
            .setColor(client.defaults.color);
        await interaction.reply({ embeds: [embed], ephemeral: true });
        dispatcher?.play();
        
        function checkURL (string) {
            try {
                new URL(string);
                return true;
            } catch (error) {
                return false;
            }
        }
    }
};