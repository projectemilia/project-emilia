const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'repeat',
    description: 'Repeats the queue or the currently playing track.',
    options: [
        {
            name: 'one',
            type: 'SUB_COMMAND',
            description: 'Loops the currently playing track.'
        },
        {
            name: 'all',
            type: 'SUB_COMMAND',
            description: 'Loops the whole queue.'
        },
        {
            name: 'off',
            type: 'SUB_COMMAND',
            description: 'Disables loop completely.'
        }
    ],

    inVc: true,
    sameVc: true,
    dispatcher: true,
    permissions: [],

    run: async (client, interaction, args, dispatcher) => {
        dispatcher.repeat = args[0];
        const embed = new MessageEmbed()
            .setDescription(`Repeat mode set to **${dispatcher.repeat}**.`)
            .setAuthor('繰り返す', null, client.cfg.url)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};