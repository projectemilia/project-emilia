const { MessageEmbed } = require('discord.js');
const Dispatcher = require('../../util/dispatcher.js');

module.exports = {
    name: 'queue',
    description: 'Shows you the queue for the server.',
    options: [],

    inVc: false,
    sameVc: false,
    dispatcher: true,
    permissions: [],

    run: async (client, interaction, args, dispatcher) => {
        const queue = dispatcher.queue.length > 9 ? dispatcher.queue.slice(0, 9) : dispatcher.queue;
        const embed = new MessageEmbed()
            .setColor(client.defaults.color)
            .setAuthor('おんがくをえんそう', null, client.cfg.url)
            .setDescription(`__Now playing__\n**[${dispatcher.current.info.title}](${dispatcher.current.info.uri})** \`[${Dispatcher.humanizeTime(dispatcher.current.info.length)}]\``)
            .setFooter(`${client.user.username} • Requested by ${dispatcher.current.user.tag} • ${dispatcher.queue.length} tracks in queue`, client.defaults.avatar);
        if (queue.length) embed.addField('Up next:', queue.map((track, index) => `**[${index + 1}]** \`${track.info.title}\``).join('\n'));
        await interaction.reply({ embeds: [ embed ], ephemeral: true });
    }
};