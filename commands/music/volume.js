const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'volume',
    description: 'Changes the volume for the player or shows the current one.',
    options: [
        {
            name: 'volume',
            description: 'Set the new volume (0% to 200%)',
            type: 'INTEGER',
            required: false
        }
    ],

    inVc: true,
    sameVc: true,
    dispatcher: true,
    permissions: [],

    run: async (client, interaction, args, dispatcher) => {
        const currentVolume = dispatcher.player.filters.volume;
        const embed = new MessageEmbed()
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setAuthor('おんりょう', null, client.cfg.url)
            .setColor(client.defaults.color);
        if (!args.length) return interaction.reply({ embeds: [embed.setDescription(`The current volume is **${currentVolume * 100}%**.`)] });
        else {
            let volume = args[0];
            if (!inRange(volume, 0, 200)) {
                volume = 100;
                embed.setDescription('The value you provided is not between **0** and **200**. \nThe volume was not changed.');
                return interaction.reply({ embeds: [embed], ephemeral: true });
            } else {
                await dispatcher.player.setVolume(volume / 100);
                embed.setDescription(`The volume was changed from **${currentVolume * 100}%** to **${volume}%**.`);
                return interaction.reply({ embeds: [embed], ephemeral: true });
            }
        }
        function inRange(x, min, max) {
            return (x - min) * (x - max) <= 0;
        }
    }
};