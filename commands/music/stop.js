const Wait = require('util').promisify(setTimeout);
const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'stop',
    description: 'Stops the music in the server and disconnects from the channel.',
    options: [],

    inVc: true,
    sameVc: true,
    dispatcher: true,

    run: async (client, interaction, args, dispatcher) => {
        dispatcher.queue.length = 0;
        dispatcher.repeat = 'off';
        dispatcher.stopped = true;
        dispatcher.player.stopTrack();
        Wait(500);
        const embed = new MessageEmbed()
            .setAuthor('さようなら', null, client.cfg.url)
            .setDescription(`Stopped the music in <#${dispatcher.player.connection.channelId}>.`)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};