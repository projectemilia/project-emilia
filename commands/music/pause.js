const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'pause',
    description: 'Pauses the currently playing track.',
    options: [],

    inVc: true,
    sameVc: true,
    dispatcher: true,

    run: async (client, interaction, args, dispatcher) => {
        const current = dispatcher.current.info;
        dispatcher.player.setPaused(true);
        const embed = new MessageEmbed()
            .setAuthor('いちじていし', null, client.cfg.url)
            .setDescription(`Paused **[${current.title}](${current.uri})**.`)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};