const { MessageEmbed } = require('discord.js');

module.exports = {
    name: 'resume',
    description: 'Resumes the playback of a paused track.',
    options: [],

    inVc: true,
    sameVc: true,
    dispatcher: true,

    run: async (client, interaction, args, dispatcher) => {
        const current = dispatcher.current.info;
        dispatcher.player.setPaused(false);
        const embed = new MessageEmbed()
            .setAuthor('さいかい', null, client.cfg.url)
            .setDescription(`Now playing **[${current.title}](${current.uri})**.`)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        return interaction.reply({ embeds: [embed], ephemeral: true });
    }
};