const { MessageEmbed } = require('discord.js');
const { inspect } = require('util');

module.exports = {
    name: 'eval',
    description: 'A dangerous command that evaluates code.',
    options: [
        {
            name: 'code',
            type: 'STRING',
            description: 'What would you like to evaluate?',
            required: true
        }
    ],

    inVc: false,
    sameVc: false,
    dispatcher: false,
    permissions: ['OWNER'],

    /* eslint-disable no-unused-vars */
    run: async (client, interaction, args, dispatcher) => {
        const code = args[0];
        let res;
        try {
            res = eval(code);
            res = inspect(res, { depth: 0 });
        } catch (error) {
            res = inspect(error, { depth: 0 });
        }
        const embed = new MessageEmbed()
            .setColor(client.defaults.color)
            .setAuthor('Results', null, client.cfg.url)
            .setDescription(`\`\`\`js\n${trim(res, 4000)}\`\`\``)
            .setTimestamp()
            .setFooter(client.defaults.footer, client.defaults.avatar);
        await interaction.reply({ embeds: [ embed ] });
        function trim (string, max) {
            return string.length > max ? string.slice(0, max) : string;
        }
    }
};