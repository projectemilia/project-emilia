const { MessageEmbed } = require('discord.js');
module.exports = async (client, message) => {
    if (message.author.bot) return;
    if (!message.guild) return;
    if (!message.guild.me.permissionsIn(message.channel).has('SEND_MESSAGES')) return;

    if (message.content === `<@!${client.user.id}>` || message.content === `<@${client.user.id}>`) {
        const embed = new MessageEmbed()
            .setAuthor('こんにちは - You mentioned me?', null, client.cfg.site)
            .setDescription('Type `/` to see a list of my commands pop up above your text box!\n*Project Emilia, by thaddeuskkr.*')
            .setColor(client.defaults.color)
            .setFooter(client.defaults.footer, client.defaults.avatar);
        return message.channel.send({ embeds: [embed] });
    }
};