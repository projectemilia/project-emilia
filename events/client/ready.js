const { version } = require('../../package.json');
const { MessageEmbed } = require('discord.js');
const fetch = require('node-fetch');

module.exports = async (client) => {
    if (process.argv.includes('--dev') || process.argv.includes('-d')) {
        client.logger.debug('Developer mode enabled. Slash commands will not be registered globally.');
        await client.application.commands.set(client.finalCommands, client.cfg.devServer);
    } else {
        await client.application.commands.set(client.finalCommands);
    }
    await client.user.setStatus('online');
    await client.user.setActivity('with Rem • ' + client.version, { type: 'PLAYING' });
    client.logger.info(`Ready in ${client.guilds.cache.size} servers with ${client.users.cache.size} users`);
    client.defaults = {
        footer: `${client.user.username} • ${client.cfg.link}`,
        color: client.cfg.color,
        avatar: client.user.avatarURL()
    };

    // Version change checking
    const oldVersion = await client.db.get('client-version');
    if (version !== oldVersion && (process.argv.includes('--autoupdate') || process.argv.includes('-au'))) {
        let res = await fetch(`https://api.github.com/repos/${client.repository}/commits/master`);
        res = await res.json();
        // console.log(res); figure this out?
        const embed = new MessageEmbed()
            .setAuthor('更新しました', null, client.cfg.url)
            .setDescription(`Automatically built source and updated to [\`${res.sha}\`](${res.html_url})\n**\`v${oldVersion} -> v${version}\`**`)
            .setFooter(client.defaults.footer, client.defaults.avatar)
            .setColor(client.defaults.color);
        client.log.send({ embeds: [embed] });
        await client.db.set('client-version', version);
    }
};