const { MessageEmbed } = require('discord.js');

module.exports = async (client, interaction) => {
    if (!interaction.isCommand()) return;
    const command = client.commands.get(interaction.commandName);
    if (!command) return interaction.reply({ content: '**A weird error occurred:** I can\'t find this command.', ephemeral: true });
    const dispatcher = client.queue.get(interaction.guildId);
    
    // Checks
    const errorEmbed = new MessageEmbed()
        .setAuthor('バカ', null, client.cfg.url)
        .setFooter(client.defaults.footer, client.defaults.avatar)
        .setColor(client.defaults.color);
    if (command.permissions && command.permissions.length > 0 && !checkPermissions(command.permissions, interaction))
        return interaction.reply({ embeds: [errorEmbed.setDescription(`You don't have the required permissions to use this command.\n**Required permissions:** \`${command.permissions.join(',')}\``)], ephemeral: true });
    if (command.inVc && !interaction.member?.voice?.channelId) 
        return interaction.reply({ embeds: [errorEmbed.setDescription('**You need to be in a voice channel to use this command!**')], ephemeral: true });
    if (command.dispatcher && !dispatcher)
        return interaction.reply({ embeds: [errorEmbed.setDescription('**There is nothing currently playing in this server!**')], ephemeral: true });
    if (command.sameVc && dispatcher && dispatcher?.player?.connection?.channelId !== interaction.member.voice.channelId) 
        return interaction.reply({ embeds: [errorEmbed.setDescription(`**You aren't in the same voice channel as the bot!** Join <#${dispatcher.player.connection.channelId}> and try again.`)], ephemeral: true });
    const args = [];
    for (let option of interaction.options.data) {
        if (option.type === 'SUB_COMMAND') {
            if (option.name) args.push(option.name);
            option.options?.forEach(x =>  {
                if (x.value) args.push(x.value);
            });
        } else if (option.value) args.push(option.value);
    }

    try {
        command.run(client, interaction, args, dispatcher);
    } catch (e) {
        interaction.reply({ content: `**Error:** \`${e.message}\`` });
        client.logger.error(`Error while running command ${interaction.commandName}:`);
        client.logger.error(e);
    }

    function checkPermissions(permissions, interaction) {
        if (permissions.includes('OWNER')) return client.cfg.owners.includes(interaction.user.id);
        else return interaction.channel.permissionsFor(interaction.member).has(permissions);
    }
};