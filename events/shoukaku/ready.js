const chalk = require('chalk');
const { MessageEmbed } = require('discord.js');
module.exports = async (client, name, resumed) => {
    client.logger.lava(`${chalk.bold(name)} >> Connected! This connection is ${resumed ? 'resumed' : 'a new connection'}.`);
    if (resumed) {
        [ ...client.shoukaku.getNode().players.keys() ].forEach(async player => {
            const dispatcher = client.queue.get(player);
            const embed = new MessageEmbed()
                .setAuthor('ごめん', null, client.cfg.url)
                .setDescription(`**Reconnected to Lavalink node \`${name}\`**\nThank you for waiting.`)
                .setColor(client.defaults.color)
                .setFooter(client.defaults.footer, client.defaults.avatar);
            await dispatcher.npMessage.edit({ embeds: [embed] }).then(message => {
                setTimeout(() => message.delete(), 10000);
            });
            
        });
    }
};