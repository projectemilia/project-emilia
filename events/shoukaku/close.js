const chalk = require('chalk');
// const { MessageEmbed } = require('discord.js');
module.exports = async (client, name, code, reason) => {
    client.logger.lava(`${chalk.bold(name)} >> Closed with code ${code} and reason ${reason}`);
    /* getNode method won't work
    client.shoukaku.getNode().players.keys().forEach(player => {
        const dispatcher = client.queue.get(player.connection.guildId);
        const embed = new MessageEmbed()
            .setAuthor('ごめん', null, client.cfg.url)
            .setDescription(`**Disconnected from Lavalink node \`${name}\`.**\nPlease wait while we attempt to reconnect.\n**Code:** \`${code}\`\n**Reason:** \`${reason}\``)
            .setColor(client.defaults.color)
            .setFooter(client.defaults.footer, client.defaults.avatar);
        dispatcher.npMessage.edit({ embeds: [embed] });
    });
    */
};