const chalk = require('chalk');
module.exports = async (client, name, error) => {
    client.logger.lava(`${chalk.bold(name)} >> Error:`);
    client.logger.lava(error);
};