FROM node:latest

WORKDIR /usr/src/
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 2309:2309
CMD [ "node", ".", "-au" ]