require('dotenv').config();
const Discord = require('discord.js');
const { Intents } = require('discord.js');
const logger = require('./util/logger.js');
const load = require('./util/loader.js');
const Queue = require('./util/queue.js');
const chalk = require('chalk');
const { Shoukaku, Libraries } = require('shoukaku');
const { LavasfyClient } = require('lavasfy');
const Keyv = require('keyv');
const config = require('./config.json');
const { version, repository } = require('./package.json');
const { DiscordTogether } = require('discord-together');

const client = new Discord.Client({
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MEMBERS,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
        Intents.FLAGS.GUILD_WEBHOOKS,
        Intents.FLAGS.GUILD_VOICE_STATES,
        Intents.FLAGS.GUILD_INVITES,
        Intents.FLAGS.GUILD_BANS,
        Intents.FLAGS.GUILD_PRESENCES,
    ]
});

let repoUrl = repository.url.replace('git+', '').replace('.git', '').replace('https://', '');
let repoPath = repoUrl.split('/');
repoPath.shift();
repoPath = repoPath.join('/');

client.commands = new Discord.Collection();
client.queue = new Queue(client);
client.dt = new DiscordTogether(client);
client.logger = logger;
client.db = new Keyv(process.env.MONGO_URL, { namespace: 'emilia-main' });
client.cfg = config;
client.version = `v${version}`;
client.repository = repoPath;
client.shoukaku = new Shoukaku(
    new Libraries.DiscordJS(client),
    [{
        name: 'Emilia',
        url: `${process.env.LAVALINK_HOST}:${process.env.LAVALINK_PORT}`,
        auth: process.env.LAVALINK_PASS
    }],
    {
        resumable: 'emilia',
        resumableTimeout: 300,
        reconnectTries: 100,
        reconnectInterval: 3000
    }
);
client.lavasfy = new LavasfyClient(
    {
        clientID: process.env.SPOTIFY_CLIENT_ID,
        clientSecret: process.env.SPOTIFY_CLIENT_SECRET
    },
    [{
        id: 'Emilia',
        host: process.env.LAVALINK_HOST,
        port: process.env.LAVALINK_PORT,
        password: process.env.LAVALINK_PASS,
        secure: false
    }]
);

load(client);

process.on('uncaughtException', async (err) => {
    logger.error(chalk.bold('Uncaught exception:'));
    console.log(err);
});

client.db.on('error', async (err) => {
    client.logger.error(chalk.bold('Keyv error:'));
    client.logger.error(err);
});

client.login(process.env.TOKEN);
