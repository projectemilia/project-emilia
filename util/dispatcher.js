const { MessageEmbed } = require('discord.js');

class Dispatcher {
    constructor({ client, guild, channel, player }) {
        this.client = client;
        this.guild = guild;
        this.channel = channel;
        this.player = player;
        this.queue = [];
        this.repeat = 'off';
        this.current = null;
        this.stopped = false;
        this.npMessage = null;

        this.player.on('start', async () => {
            if (this.repeat === 'one') return;
            const embed = new MessageEmbed()
                .setColor(this.client.defaults.color)
                .setAuthor(
                    `${this.current.info.title} [${Dispatcher.humanizeTime(this.current.info.length)}]`, 
                    `https://img.youtube.com/vi/${this.current.info.identifier}/default.jpg`, 
                    this.current.info.uri
                )
                .setFooter(`${this.client.user.username} • Requested by ${this.current.user.tag} • 🔈 ${this.client.channels.cache.get(this.current.vc).name}`, this.client.defaults.avatar);
            this.npMessage = await this.channel
                .send({ embeds: [ embed ] })
                .catch(() => null);
        });
        this.player.on('end', () => {
            if (this.repeat === 'one') this.queue.unshift(this.current);
            if (this.repeat === 'all') this.queue.push(this.current);
            this.npMessage?.delete().catch(() => null);
            this.play();
        });
        for (const event of ['closed', 'error']) {
            this.player.on(event, data => {
                if (data instanceof Error || data instanceof Object) this.client.logger.error(data);
                this.queue.length = 0;
                this.destroy();
            });
        }
    }

    static humanizeTime(ms) {
        const seconds = Math.floor(ms / 1000 % 60);
        const minutes = Math.floor(ms / 1000 / 60 % 60);
        return [ minutes.toString().padStart(2, '0'), seconds.toString().padStart(2, '0') ].join(':');
    }

    get exists() {
        return this.client.queue.has(this.guild.id);
    }

    async play() {
        if (!this.exists || !this.queue.length) return this.destroy();
        this.current = this.queue.shift();
        if (this.current.resolve) {
            const oldCurrent = this.current;
            this.current = await this.current.resolve();
            this.current.user = oldCurrent.user;
            this.current.vc = oldCurrent.vc;
        }
        this.player
            .setVolume(1)
            .playTrack(this.current.track);
    }
    
    destroy() {
        this.queue.length = 0;
        this.player.connection.disconnect();
        this.client.queue.delete(this.guild.id);
        if (this.stopped) return;
        // insert queue end message if any
    }
}
module.exports = Dispatcher;
