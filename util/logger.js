const chalk = require('chalk');
module.exports = {
    debug: (details) => {
        console.log(stamps() + chalk.bgMagenta(chalk.black(' DEBUG ')) + ' ' + (details || ''));
    },
    info: (details) => {
        console.log(stamps() + chalk.bgGreen(chalk.black(' INFO ')) + ' ' + (details || ''));
    },
    warn: (details) => {
        console.log(stamps() + chalk.bgYellow(chalk.black(' WARN ')) + ' ' + (details || ''));
    },
    error: (details) => {
        console.log(stamps() + chalk.bgRed(chalk.black(' ERROR ')) + ' ' + (details || ''));
    },
    critical: (details) => {
        console.log(stamps() + chalk.bgRed(chalk.black(' CRITICAL ')) + ' ' + (details || ''));
    },
    lava: (details) => {
        console.log(stamps() + chalk.bgWhite(chalk.black(' LAVA ')) + ' ' + (details || ''));
    }
};
function stamps () {
    const date = new Date();
    const text = chalk.bgCyan(chalk.black(` ${dd(date.getDate())}/${dd(date.getMonth() + 1)}/${dd(date.getFullYear())} `)) + chalk.bgWhite(chalk.blue(` ${dd(date.getHours())}:${dd(date.getMinutes())} `));
    return text;
}
function dd (initial) {
    let final;
    if (initial.toString().length == 1) {
        final = '0' + initial;
    } else {
        final = initial;
    }
    return final;
}