const Dispatcher = require('./dispatcher.js');

class Queue extends Map {
    constructor(client, iterable) {
        super(iterable);
        this.client = client;
    }

    async handle(guild, member, channel, node, track) {
        track.user = member.user;
        track.vc = member.voice.channelId;
        const existing = this.get(guild.id);
        if (!existing) {
            const player = await node.joinChannel({
                guildId: guild.id,
                shardId: guild.shardId,
                channelId: member.voice.channelId
            });
            const dispatcher = new Dispatcher({
                client: this.client,
                guild,
                channel,
                player
            });
            dispatcher.queue.push(track);
            dispatcher.npMessage = null;
            this.set(guild.id, dispatcher);
            return dispatcher;
        }
        existing.queue.push(track);
        return null;
    }
}
module.exports = Queue;