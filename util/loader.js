const fs = require('fs');
const ascii = require('ascii-table');
const { WebhookClient } = require('discord.js');

module.exports = async (client) => {
    const final = [];
    const table = new ascii().setHeading('Commands', 'Status');
    const categories = fs.readdirSync('./commands');
    client.categories = categories;
    for (const category of categories) {
        const commands = fs.readdirSync(`./commands/${category}`).filter((file) => file.endsWith('.js'));
        for (const command of commands) {
            const cmd = require(`../commands/${category}/${command}`);
            if (!cmd.name) {
                table.addRow(command.replace('.js', ''), '✘ » Command name not found');
                continue;
            } else if (!cmd.description) {
                table.addRow(command.replace('.js', ''), '✘ » Command description not found');
            } else {
                client.commands.set(cmd.name, cmd);
                final.push(cmd);
                table.addRow(command.replace('.js', ''), '✓ » Loaded');
            }
        }
    }
    const tbl = table.toString();
    const lines = tbl.split('\n');
    lines.forEach(line => {
        client.logger.info(line);
    });
    client.finalCommands = final;
    const clientEvents = fs.readdirSync('./events/client').filter((file) => file.endsWith('.js'));
    const shoukakuEvents = fs.readdirSync('./events/shoukaku').filter((file) => file.endsWith('.js'));
    for (const file of clientEvents) {
        const event = require(`../events/client/${file}`);
        client.on(file.split('.')[0], event.bind(null, client));
    }
    for (const file of shoukakuEvents) {
        const event = require(`../events/shoukaku/${file}`);
        client.shoukaku.on(file.split('.')[0], event.bind(null, client));
    }

    // Lavasfy setup
    await client.lavasfy.requestToken();

    client.log = new WebhookClient({ id: process.env.WEBHOOK_ID, token: process.env.WEBHOOK_TOKEN });
};